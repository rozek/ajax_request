const textContentLoader = {
    btn : btn = document.querySelector('.get-json'),
    btnClear : btnClear = document.querySelector('.clear-btn'),
    field: field = document.querySelector('#response'),
    getJSON: getJSON => {
        const xhr = new XMLHttpRequest();
        xhr.onload = () => {
            const parseUsers = JSON.parse(xhr.responseText);
            console.log(parseUsers);
            
            (xhr.status === 200)?field.textContent = parseUsers:console.log(`status to ${xhr.status}`)
            let response = '';
            parseUsers.map( user => response += `Username: ${user.username},<br> Email: ${user.email} <br><br>`);
            this.field.innerHTML = response;
        }
        xhr.open('GET', 'js/users.json', true);
        xhr.send('');

    },
    clearContent: clearContent => { field.textContent = ''}
}
textContentLoader.btn.addEventListener('click', textContentLoader.getJSON);
textContentLoader.btnClear.addEventListener('click', textContentLoader.clearContent);
