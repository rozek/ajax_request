const textContentLoader = {
    btn : btn = document.querySelector('.get-json'),
    btnClear : btnClear = document.querySelector('.clear-btn'),
    field: field = document.querySelector('#response'),
    getJSON: getJSON => {
        const xhr = new XMLHttpRequest();
        const url = 'http://michalroszko.com/JSON/data.php';
        xhr.onload = () => {
            const userData = JSON.parse(xhr.responseText);
            const address = userData.data.website;
            let response = '';
            userData.data.endpoints.map( endpoint => response += `Endpoint: ${endpoint},<br>`);
            field.innerHTML = (`${response} <br> ${address}`);
        }
        xhr.open('GET', url, true);
        xhr.send('');

    },
    clearContent: clearContent => { field.textContent = ''}

}
textContentLoader.btn.addEventListener('click', textContentLoader.getJSON);
textContentLoader.btnClear.addEventListener('click', textContentLoader.clearContent);
