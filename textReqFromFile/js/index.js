const textContentLoader = {
    btn : btn = document.querySelector('#get-btn'),
    btnClear : btnClear = document.querySelector('#clear-btn'),
    field: field = document.querySelector('#response'),
    getAjax: getAjax => {
        const xhr = new XMLHttpRequest();
        xhr.onload = () => {
            (xhr.status === 200)?field.textContent = xhr.responseText:console.log(`status to ${xhr.status}`)
        }
        xhr.open('GET', 'text.txt', true);
        xhr.send('');
        // console.log(textContentLoader.btn);
    },
    clearContent: clearContent => { field.textContent = ''}
}
textContentLoader.btn.addEventListener('click', textContentLoader.getAjax);
textContentLoader.btnClear.addEventListener('click', textContentLoader.clearContent);